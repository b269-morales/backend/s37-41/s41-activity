const express = require("express");

// Creates a router instance that functions as a middleware and routing system
const router = express.Router();

const userController = require("../controllers/userController.js");


const auth = require("../auth");

router.post("/checkEmail", (req,res) => {
	userController.checkEmailExists(req.body).then(
		resultFromController => res.send(resultFromController)
		);
});

router.post("/register", (req,res) => {
	userController.registerUser(req.body).then(
		resultFromController => res.send(resultFromController)
		);
});

router.post("/login", (req,res) => {
	userController.loginUser(req.body).then(
		resultFromController => res.send(resultFromController)
		);
});

router.post("/details", auth.verify, (req,res) => {

	const userData = auth.decode(req.headers.authorization);

	userController.getProfile({userId: userData.id}).then(
		resultFromController => res.send(resultFromController)
		);
});

router.get("/getAllUser", (req,res) => {
	userController.getAllUser().then(
		resultFromController => res.send(resultFromController)
		);
});

router.post("/enroll", auth.verify, (req,res) => {
	const data = {
		userId: auth.decode(req.headers.authorization).id,
		courseId: req.body.courseId
	}
	userController.enroll(data).then(resultFromController => res.send(resultFromController));
});

router.post("/unenroll", auth.verify, (req,res) => {
	const data = {
		userId: auth.decode(req.headers.authorization).id,
		courseId: req.body.courseId
	}
	userController.unenroll(data).then(resultFromController => res.send(resultFromController));
});



module.exports = router;