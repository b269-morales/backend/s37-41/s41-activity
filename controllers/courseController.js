const Course = require("../models/Course.js");
const User = require("../models/User.js");

const bcrypt = require("bcrypt");

const auth = require("../auth");


/*module.exports.addCourse = (reqBody, user) => {
	return new Promise((resolve, reject) => {
		if (!user.isAdmin) {
			reject("User needs to be admin to add course");
		} else {
			Course.find({ name: reqBody.name }).then((result) => {
				if (result.length > 0) {
					reject(`Course ${reqBody.name} is already registered`);
				} else {
					let newCourse = new Course({
						name: reqBody.name,
						description: reqBody.description,
						price: reqBody.price,
					});

					newCourse.save().then((course, error) => {
						if (error) {
							reject(false);
						} else {
							resolve(true);
						}
					});
				}
			});
		}
	});
};
*/
module.exports.addCourse = (data) => {
	// User is an admin
	if (data.isAdmin) {
		// Creates a variable "newCourse" and instantiates a new "Course" object using the mongoose model
		// Uses the information from the request body to provide all the necessary information
		let newCourse = new Course({
			name : data.course.name,
			description : data.course.description,
			price : data.course.price
		});
		// Saves the created object to our database
		return newCourse.save().then((course, error) => {
			// Course creation successful
			if (error) {
				return false;
			// Course creation failed
			} else {
				return true;
			};
		});
	} 
	// User is not an admin
	let message = Promise.resolve(`User must be ADMIN to access this!`);
	return message.then((value) => {
		return {value}
	})
};


module.exports.getAllCourses = () => {
	return Course.find({}).then(result => {
		return result;
	})
}

module.exports.getActiveCourses = () => {
	return Course.find({isActive: true}).then(result => {
		return result;
	})
}


module.exports.getCourse = (reqParams) =>{
	return Course.findById(reqParams.courseId).then(result =>{
		return result;
	})
};

// Update a course

module.exports.updateCourse = (reqParams, reqBody) => {
	let updatedCourse = {
		name : reqBody.name,
		description : reqBody.description,
		price : reqBody.price
	};

	// 'findByIdAndUpdate' (document ID, updatesToBeApplied)
	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course,error) => {
		if (error) {
			return false;
		} else {
			return true;
		}
	})
}

module.exports.archiveCourse = (data) => {
	if (data.isAdmin) {
		let archiveCourse = {
			isActive: false
		};
		return Course.findByIdAndUpdate(data.courseId, archiveCourse).then((course,error) => {
			if (error) {
				return false;
			} else {
				return true;
			}
		})
	};	
	// User is not an admin
	let message = Promise.resolve(`User must be ADMIN to access this!`);
	return message.then((value) => {
		return {value}
	});
}


module.exports.enableCourse = (data) => {
	if (data.isAdmin) {
		let archiveCourse = {
			isActive: true
		};
		return Course.findByIdAndUpdate(data.courseId, archiveCourse).then((course,error) => {
			if (error) {
				return false;
			} else {
				return true;
			}
		})
	};	
	// User is not an admin
	let message = Promise.resolve(`User must be ADMIN to access this!`);
	return message.then((value) => {
		return {value}
	});
}
