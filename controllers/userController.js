const User = require("../models/User.js");
const Course = require("../models/Course.js");

const bcrypt = require("bcrypt");

const auth = require("../auth");

// Check if email already exists
/*
Business Logic:
1) Use mongoose "find" method to find duplicate emails
2) Use the "then" method to send a response back to the Postman application based on the result of the "find" method
*/
module.exports.getAllUser = () => {
	return User.find({}).then(result => {
		return result;
	})
};


module.exports.checkEmailExists = (reqBody) => {
	return User.find({ email: reqBody.email }).then((result) => {
		if (result.length > 0) {
			return true;
		} 
		else {
			return false;
		}
	});
};

/*
BUSINESS LOGIC
1) Create a new User object using the mongoose model and the information from the request body
2) Make sure that the password is encrypted
3) Save the new User to the database
*/

module.exports.registerUser = (reqBody) => {
	return User.find({ $or: [{ email: reqBody.email}, {mobileNo: reqBody.mobileNo}]}).then((result) => {
		if (result.length > 0)
		{	
			return `Email or mobile number is already registered`;;
		} else {
			let newUser = new User ({
				firstName: reqBody.firstName,
				lastName: reqBody.lastName,
				email: reqBody.email,
				mobileNo: reqBody.mobileNo,
				isAdmin: reqBody.isAdmin,
			password: bcrypt.hashSync(reqBody.password, 15) // "bcrypt.hashSync" is a function in the bycrpt library that is used to generate hash values for a given input string synchronously. "reqBody.password" - input string that needs to be hashed. "10" - value provided by the dev as to the number of 'salt' rounds that the bcrypt algorithm will run in order to encrypt password
		});

			return newUser.save().then((user,error) => {
				if (error){
					return false;
				} else {
					return true;
				};
			})
		};

	})
};

// User Authentication
/*
Business Logic:
1) Check the database if the user email exists
2) Compare the password provided in the login form with the password stored in the database
3) Generate/return a JSON web token if the user is successfully logged in and return false if not
*/

module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result =>
	{
		if (result == null){
			return false;
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
			if (isPasswordCorrect){
				return {access: auth.createAccessToken(result)
			}
		} else {
			return false;
		}
	}
})
};



// Retrieve User Details
/*
Business Logic:
1. Find the document in the database using the user's ID
2. Reassign the password of the returned document to an empty string
3. Return the result back to the frontend/postman
*/

module.exports.getProfile = (reqBody) =>{
	return User.findById(reqBody.userId).then(result =>{
		result.password = "";
		return result;
	})
};

module.exports.enroll = async (data) => {
	try {
	// Check if the course is active
		const course = await Course.findById(data.courseId);
		if (!course.isActive){
			return `Course is not active`
		}
	
		// Check if user is already enrolled in the course
		const user = await User.findById(data.userId);
		if (user.enrollments.some(enrollment => enrollment.courseId.toString() === data.courseId.toString())) {
			return `User is already enrolled in the course`;
		}
	
		// Add course ID in the enrollments array of the user
		let isUserUpdated = await User.findById(data.userId).then(user => {
			user.enrollments.push({courseId: data.courseId});
			return user.save().then((user,error) => {
				if (error) {
					return false;
				} else {
					return true;
				}
			})
		})
	
		// Add user ID in the enrollees array of the course
		let isCourseUpdated = await Course.findById(data.courseId).then(course => {
			course.enrollees.push({userId: data.userId});
			return course.save().then((course, error) => {
				if (error) {
					return false;
				} else {
					return true;
				}
			})
		})
	
		if(isUserUpdated && isCourseUpdated){
			return true;
		} else{
			return false;
		}
	} catch (error) {
		return "UserID or CourseID does not exist"
	}
}



module.exports.unenroll = async (data) => {
		const user = await User.findById(data.userId);
		const courseIndex = user.enrollments.findIndex(enrollment => enrollment.courseId.toString() === data.courseId.toString());
		if (courseIndex === -1) {
			return `User is not enrolled in the course`;
		}
	
		user.enrollments.splice(courseIndex, 1);
		let isUserUpdated = await user.save().then((user, error) => {
			if (error) {
				return false;
			} else {
				return true;
			}
		});
	
		const course = await Course.findById(data.courseId);
		const courseEnrolleeIndex = course.enrollees.findIndex(enrollee => enrollee.userId.toString() === data.userId.toString());
		course.enrollees.splice(courseEnrolleeIndex, 1);
		let isCourseUpdated = await course.save().then((course, error) => {
			if (error) {
				return false;
			} else {
				return true;
			}
		});
	
		if (isUserUpdated && isCourseUpdated) {
			return true;
		} else {
			return false;
		}
	
}
